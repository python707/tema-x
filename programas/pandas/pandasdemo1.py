import pandas as pd

data = {
  "Empleado": ["Ernesto Jiménez", "Sandra López", "Hugo Sánchez"],
  "Calificación": [90, 75, 85]
}

myvar = pd.DataFrame(data)

print(myvar)