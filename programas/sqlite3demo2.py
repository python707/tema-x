
import sqlite3 as sql3

cnn=sql3.Connection('database.db')

cnn.execute("delete from detalle")
cnn.execute("delete from ventas")
cnn.execute("delete from productos")
cnn.execute("insert into productos values(1,'Producto 1',200,14.5)")
cnn.execute("insert into productos values(2,'Producto 2',100,35)")
cnn.execute("insert into productos values(3,'Producto 3',20,15)")
cnn.execute("insert into productos values(4,'Producto 4',100,55)")
cnn.execute("insert into productos values(5,'Producto 5',250,105)")
cnn.execute("insert into productos values(6,'Producto 6',290,145)")
cnn.execute("insert into productos values(7,'Producto 7',210,150)")
cnn.execute("insert into productos values(8,'Producto 8',150,135)")

cnn.execute("insert into ventas values(1,'2023-09-23')")
cnn.execute("insert into ventas values(2,'2023-10-03')")
cnn.execute("insert into ventas values(3,'2023-10-03')")
cnn.execute("insert into ventas values(4,'2023-10-10')")

cnn.execute("insert into detalle values(1,4,3)")
cnn.execute("insert into detalle values(1,5,1)")
cnn.execute("insert into detalle values(2,3,1)")
cnn.execute("insert into detalle values(2,4,1)")
cnn.execute("insert into detalle values(2,7,2)")
cnn.execute("insert into detalle values(3,4,3)")
cnn.execute("insert into detalle values(3,1,3)")
cnn.execute("insert into detalle values(4,2,2)")
cnn.execute("insert into detalle values(4,1,6)")
cnn.commit()
cnn.close()

print('Se ha llenado la base de datos!!')