


import sqlite3 as sql3  

cnn=sql3.Connection('database.db')

cnn.execute('create table productos(id int primary key, desc varchar(40),existencias int, precio float)')
cnn.execute('create table ventas(id_venta int primary key, fecha date)')
cnn.execute('create table detalle(id_venta int references ventas(id_venta), id int references productos(id), cantidad int, primary key(id_venta,id))')

cnn.close()

print('La base de datos se ha creado correctamente!!')
